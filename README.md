# ReactJS_Study_routing

This README would normally document whatever steps are necessary to get your application up and running.

### How to clone source code

- git clone https://xditaba@bitbucket.org/xditaba/reactjs_routing.git

### How to remote source code

- git remote add origin https://xditaba@bitbucket.org/xditaba/reactjs_routing.git
- git push -u origin master

## Review content

- Route parameters

  > https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/8140669#overview
  >
  > > Parsing Query Parameters & the Fragment
  > >
  > > https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/URLSearchParams

- Absolute & relative paths

  > - https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/8140667#overview
  >
  > * Relative Path Vs Absolute Path

- `withRouter`
- Nested routes
- Redirect
- Handling 404 case
